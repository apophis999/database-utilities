CREATE OR REPLACE FUNCTION build.create_schema(
    new_schema character varying,
    create_log boolean DEFAULT true)
  RETURNS void AS
$BODY$
declare
begin
    execute 'create schema if not exists '||new_schema||' authorization builder;';
    perform build.create_schema_privileges(new_schema);

    if (create_log) then
        execute 'create schema if not exists '||new_schema||'_log authorization builder;';
        perform build.create_schema_privileges(new_schema||'_log');
    end if;

end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION build.create_schema(character varying, boolean)
  OWNER TO builder;