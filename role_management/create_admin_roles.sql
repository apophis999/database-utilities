create role builder with login createrole password 'myPassword';
create role builder_super with nologin superuser noinherit;
create role logger with nologin;
grant logger to builder;
grant builder_super to builder;