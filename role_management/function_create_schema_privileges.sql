CREATE OR REPLACE FUNCTION build.create_schema_privileges(new_schema character varying)
  RETURNS void AS
$BODY$
declare
begin
    raise notice 'build.create_schema_privileges(%)  starting', new_schema;

    if not exists (select 1 from information_schema.schemata where schema_name = new_schema) then
        raise 'schema % doesn''t exist', new_schema using errcode = '42704';
        return;
    end if;

    perform build.create_role(new_schema||'_ro');
    perform build.create_role(new_schema||'_rw');

    if (position('_log' in  new_schema) = (length(new_schema) - 3)) then
        execute 'grant '||new_schema||'_rw to logger;';
        raise notice 'granted rw to logger';
    else
        execute 'grant '||new_schema||'_ro to global_ro;';
        execute 'grant '||new_schema||'_rw to global_rw;';
        raise notice 'added schema roles to global_ro and global_rw';
    end if;

    execute 'alter default privileges for user builder in schema '||new_schema||' grant select on tables to '||new_schema||'_ro;';
    execute 'alter default privileges for user builder in schema '||new_schema||' grant insert, select, update, delete on tables to '||new_schema||'_rw;';
    execute 'alter default privileges for user builder in schema '||new_schema||' grant select, update, usage on sequences to '||new_schema||'_rw;';
    execute 'alter default privileges for user db_builder in schema '||new_schema||' grant execute on functions to '||new_schema||'_rw;';
    execute 'grant all on schema '||new_schema||' to '||new_schema||'_rw;';
    execute 'grant usage on schema '||new_schema||' to '||new_schema||'_ro;';
    execute 'alter schema '||new_schema||' owner to builder;';
    raise notice 'Created default privileges for schema';
end;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION build.create_schema_privileges(character varying)
  OWNER TO builder_super;