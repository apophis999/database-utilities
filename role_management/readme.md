Role Management Concepts in a Microservices / Microdatabase architecture
If operating within the context that services own databases,features own schemas,
and permissions are controlled in general at the schema layer.

Base concepts:
DB Manager Role (named builder)
owns the database and the objects / artifacts
is a login role
is not a super user role

A Logger role (named logger)
For change data capture / audit log triggers

A super user role (named builder_super)
with no login
the manager role inherits and can leverage the set role mechanism to escalate
permissions when needed

Schema Permissions
each schema has two group roles, one read only (RO) and one read/write (RW)
all tables automatically inherit the schema permissions
application login users and generic human users are assigned (or not) the schema
level roles to grant privileges

Change Data Capture
The underlying framework optionally (or by default) will generate change data
capture (CDC) against the base tables.  This works by leveraging PostgreSQLs
inheritance mechanisms.