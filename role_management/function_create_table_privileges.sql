CREATE OR REPLACE FUNCTION build.create_table_privileges(
    schema_name character varying,
    table_name character varying)
  RETURNS void AS
$BODY$
declare
    row record;
begin
    -- Set default table permissions
    execute 'alter table '||schema_name||'.'||table_name||' owner to builder;';
    execute 'grant all on table '||schema_name||'.'||table_name||' to builder;';
    execute 'grant select on table '||schema_name||'.'||table_name||' to '||schema_name||'_ro;';
    execute 'grant select, update, insert, delete on table '||schema_name||'.'||table_name||' to '||schema_name||'_rw;';
    execute 'grant select on table '||schema_name||'.'||table_name||' to '||schema_name||'_ro;';

    -- Look for sequences and adjust permissions accordingly
    for row in execute $$
        select sn.nspname as sequence_namespace, s.relname as sequence_name
        from pg_class t
        join pg_namespace tn on t.relnamespace = tn.oid
        join pg_depend d on d.refobjid = t.oid
        join pg_class s on d.objid = s.oid
        join pg_namespace sn on s.relnamespace = sn.oid
        where tn.nspname = $1 and t.relname = $2 and s.relkind = 'S'
    $$
    using schema_name, table_name
    loop
        execute 'grant usage, select on sequence '||row.sequence_namespace||'.'||row.sequence_name||' to '||schema_name||'_rw';
        execute 'grant all on sequence '||row.sequence_namespace||'.'||row.sequence_name||' to builder';
    end loop;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION build.create_table_privileges(character varying, character varying)
  OWNER TO builder_super;