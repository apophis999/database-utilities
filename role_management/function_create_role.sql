create or replace function build.create_role(
    role_name character varying,
    is_group_role boolean default true,
    role_password character varying default null,
    force_password_change boolean default false)
  returns void as
$BODY$
declare
    v_role_exists boolean;
begin
    -- Check if role exists
    select count(1) >= 1
    from pg_catalog.pg_authid
    where rolname = role_name
    into strict v_role_exists;

    -- Check the password is set
    if is_group_role = false and role_password is null then
        raise exception 'Null password not allowed' using errcode = '22004';
    end if;

    -- Take appropriate action
    case
    when not v_role_exists and is_group_role then
        -- Create a group role
        execute format('create role %I with nologin', role_name);
    when not v_role_exists and not is_group_role then
        -- Create a login role with the specified password
        execute format('create role %I with login password %L', role_name, role_password);
    when v_role_exists and not is_group_role and force_password_change then
        -- Change existing login role's password
        raise notice 'Role % exists, changing password', role_name;
        execute format('alter role %I with password %L', role_name, role_password);
    else
        raise notice 'Skipping - Role % exists', role_name;
    end case;

end;
$BODY$
  language plpgsql volatile security definer;
alter function build.create_role(character varying, boolean, character varying, boolean)
  owner to builder_super;