-- FUNCTION: build.create_table(character varying, character varying, boolean)

-- DROP FUNCTION build.create_table(character varying, character varying, boolean);

CREATE OR REPLACE FUNCTION build.create_table(
	new_table_schema character varying,
	new_table_name character varying,
	create_log boolean DEFAULT false)
    RETURNS void
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
    the_tables text[][];
    the_table text[];
    the_columns text[][];
    the_column text[];
    is_empty_table boolean;
    log_table_exists boolean;
    log_trigger_exists boolean;
    truncate_trigger_exists boolean;
    append_trigger_exists boolean;
    pkcol_int_exists boolean;
    pkcol_int_type text;
    pkcol_uuid_exists boolean;
begin

   
    the_tables := array[[new_table_schema, new_table_name]];
    --end if;

    <<BIGLOOP>>
    foreach the_table slice 1 in array the_tables
    loop
    raise notice 'Current Table: %.%', the_table[1], the_table[2];

    --
    -- Control flags
    --

    -- Determine if table is empty
    execute 'select count(1) = 0 from '||(the_table::text[])[1]||'.'||the_table[2]
        into strict is_empty_table;

    -- Determine if log table exits
    execute 'select count(1) = 1 from information_schema.tables where table_schema='''||the_table[1]
        ||'_log'' and table_name='''||the_table[2]||'_log'''
        into strict log_table_exists using the_table[1], the_table[2];

    -- Determine if integer primary key candidate exists
    execute format('select count(1) = 1 from information_schema.columns '
            || 'where table_schema = $1 '
            || 'and table_name = $2 '
            || 'and column_name = $3 '
            || 'and data_type in (''smallint'', ''integer'', ''bigint'') '
            )
        into strict pkcol_int_exists
        using the_table[1], the_table[2], the_table[2]||'_key';

    -- Determine integer type if exists
    if( pkcol_int_exists) then
    execute format('select data_type from information_schema.columns '
            || 'where table_schema = $1 '
            || 'and table_name = $2 '
            || 'and column_name = $3 '
            || 'and data_type in (''smallint'', ''integer'', ''bigint'') '
            )
        into strict pkcol_int_type
        using the_table[1], the_table[2], the_table[2]||'_key';
    end if;

    -- Determine if uuid primary key candidate exists
    execute 'select count(1) = 1 from information_schema.columns
             where table_schema = '''||the_table[1]||'''
             and table_name = '''||the_table[2]||'''
             and column_name = '''||the_table[2]||'_id''
             and data_type = ''uuid'''
        into strict pkcol_uuid_exists using the_table[1], the_table[2];
/*
    -- Determine if the truncate no op trigger exists
    execute 'select exists(select tgname
        from pg_catalog.pg_trigger
        where tgrelid = '''||the_table[1]||'.'||the_table[2]||'''::regclass
        and tgfoid = ''common.protect_table_no_op()''::regprocedure)' into strict truncate_trigger_exists;

    -- Determine if append only trigger exists on log table
    if (log_table_exists) then
        execute 'select exists(select tgname
            from pg_catalog.pg_trigger
            where tgrelid = '''||the_table[1]||'_log.'||the_table[2]||'_log''::regclass
            and tgfoid = ''common.protect_table_append_only()''::regprocedure)' into strict append_trigger_exists;
    else
        append_trigger_exists := false;
    end if;
*/
    --
    -- Create log table with inheritance
    --

    -- If want logging and log table doesn't exists
/*    if (create_log and not log_table_exists) then
        -- If table is non-empty then can't move
        if (not is_empty_table) then
            raise exception 'Non-empty table detected' using errcode = '55000';
        end if;
        -- Move table to be the log table
        execute 'alter table '||the_table[1]||'.'||the_table[2]||' set schema '||the_table[1]||'_log';
        execute 'alter table '||the_table[1]||'_log.'||the_table[2]||' rename to '||the_table[2]||'_log';

        -- Create a new child table that inherits from the parent log table
        if( pkcol_uuid_exists ) then
            execute format('create table %I.%I (%I uuid primary key '
                || ', user_name char varying(25) default session_user '
                || ', transaction_period tstzrange default tstzrange(current_timestamp, NULL::timestamp with time zone)'
                || ') inherits (%I.%I);'
                , the_table[1], the_table[2], the_table[2]||'_id', the_table[1]||'_log', the_table[2]||'_log'
            );
        elsif ( pkcol_int_exists ) then
            execute format('create table %I.%I (%I %s primary key '
                || ', user_name char varying(25) default session_user '
                || ', transaction_period tstzrange default tstzrange(current_timestamp, NULL::timestamp with time zone)'
                || ') inherits (%I.%I);'
                , the_table[1], the_table[2], the_table[2]||'_key'
                , case pkcol_int_type
                    when 'smallint' then 'smallserial'
                    when 'integer' then 'serial'
                    when 'bigint' then 'bigserial'
                  end
                , the_table[1]||'_log', the_table[2]||'_log'
            );
        else
            raise exception 'No candidate primary key column found.' using errcode = '55000';
        end if;

    -- If want logging, but log table already exists
    elsif (create_log and log_table_exists) then
        -- Make sure the child table inherits from the parent log table
        if (not exists (
            WITH RECURSIVE inh AS (
                SELECT i.inhrelid
                FROM pg_catalog.pg_inherits i
                WHERE inhparent = (the_table[1]||'_log.'||the_table[2]||'_log')::regclass
                UNION
                SELECT i.inhrelid
                FROM inh
                INNER JOIN pg_catalog.pg_inherits i ON (inh.inhrelid = i.inhparent)
            )
            SELECT pg_namespace.nspname, pg_class.relname
                FROM inh
                INNER JOIN pg_catalog.pg_class ON (inh.inhrelid = pg_class.oid)
                INNER JOIN pg_catalog.pg_namespace ON (pg_class.relnamespace = pg_namespace.oid)
        )) then
            execute 'ALTER TABLE '||the_table[1]||'.'||the_table[2]||' INHERIT '||the_table[1]||'_log.'||the_table[2]||'_log;';
        end if;
    end if;
*/
    --
    -- Check all required columns exist
    --
/*
    -- Check tables
    if (create_log) then
        the_columns := array[['user_name', 'char varying(25)','_log'],
            ['transaction_period', 'tstzrange','_log']];
    else
        the_columns := array[[]];
    end if;
    */
/*
    <<LOGCOLUMNS>>
    foreach the_column slice 1 in array the_columns
    loop
        if (not exists (
            select 1
            from information_schema.columns
            where table_schema = the_table[1]||the_column[3]
                and table_name = the_table[2]||the_column[3]
                and column_name = the_column[1]
        )) then
            execute 'alter table '||the_table[1]||the_column[3]||'.'||the_table[2]||the_column[3]
                ||' add column '||the_column[1]||' '||the_column[2];
        end if;
    end loop LOGCOLUMNS;
*/
    --
    -- Setup Constraints
    --

    -- Check if current table has primary key
    if (not exists (
        select 1
        from pg_catalog.pg_constraint
        where contype = 'p'
        and conrelid = (the_table[1]||'.'||the_table[2])::regclass
    )) then
        execute 'alter table '||the_table[1]||'.'||the_table[2]||' add primary key ('||the_table[2]||'_key)';
    end if;
/*
    -- Check log table has exclusion over transaction_period
    if (create_log and not exists(
        select 1
        from pg_catalog.pg_constraint
        where contype = 'x'
            and conrelid = (the_table[1]||'_log.'||the_table[2]||'_log')::regclass
            and conkey = (
                with keyarray as (
                    select attnum
                    from pg_catalog.pg_attribute
                    where attrelid = (the_table[1]||'_log.'||the_table[2]||'_log')::regclass
                    and attname = the_table[2]
                        || case when pkcol_uuid_exists then '_id'
                            when pkcol_int_exists then '_key' end
                )
                select array[ka.attnum, pa.attnum]
                from pg_catalog.pg_attribute pa
                cross join keyarray ka
                where attrelid = (the_table[1]||'_log.'||the_table[2]||'_log')::regclass
                and attname = 'transaction_period'
            )
            and conexclop = array['=(bigint,bigint)'::regoperator::oid, '&&(anyrange,anyrange)'::regoperator::oid]
    )) then
        if (pkcol_uuid_exists) then
            execute format('alter table %I.%I add exclude using gist (cast( %I as text) with ='
                || ', transaction_period with&&)'
                , the_table[1]||'_log', the_table[2]||'_log', the_table[2]||'_id');
        elsif (pkcol_int_exists) then
            execute format('alter table %I.%I add exclude using gist (%I with =, transaction_period with &&)'
                , the_table[1]||'_log', the_table[2]||'_log', the_table[2]||'_key');
        end if;
    end if;
*/

    --
    -- Setup necessary triggers
    --
/*
    -- Add log row trigger
    execute 'select exists(select tgname
        from pg_catalog.pg_trigger
        where tgrelid = '''||the_table[1]||'.'||the_table[2]||'''::regclass
        and tgfoid = ''common.log_row_change()''::regprocedure)' into strict log_trigger_exists;

    if (create_log and not log_trigger_exists) then
        execute 'create trigger log_'||the_table[2]||'
            before insert or update or delete
            on '||the_table[1]||'.'||the_table[2]||'
            for each row
            execute procedure common.log_row_change()';
    end if;
*/
    --
    -- Add protection triggers
    --

    -- Truncate no operation on current table
    /*if (not truncate_trigger_exists) then
        execute 'create trigger '||the_table[2]||'_truncate_no_op
            before truncate
            on '||the_table[1]||'.'||the_table[2]||'
            for each statement
            execute procedure common.protect_table_no_op()';
    end if;

    -- Make log table append only
    if (create_log and not append_trigger_exists) then
        execute 'create trigger '||the_table[2]||'_log_append_only
            before update or delete or truncate
            on '||the_table[1]||'_log.'||the_table[2]||'_log
            for each statement
            execute procedure common.protect_table_append_only()';
    end if;
*/
    --
    -- Set proper privileges
    --

    execute 'select build.create_table_privileges('''||the_table[1]||''','''||the_table[2]||''')';
/*
    if (create_log) then
        execute 'select build.create_table_privileges('''||the_table[1]||'_log'','''||the_table[2]||'_log'')';
    end if;
    */

    end loop BIGLOOP;

end;

$BODY$;

ALTER FUNCTION build.create_table(character varying, character varying, boolean)
    OWNER TO builder;
