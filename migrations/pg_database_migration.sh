#!/usr/bin/env bash

#USAGE is database_pullback.sh <database_name/datname> <environment to bring back data to>

start=`date +%s`
#########################################   ENVIRONMENT   ############################################
#SET VERBOSE = 1 for debugging
VERBOSE=1;
##### ENVIRONMENTS
PG_USER='adminstration_user'
PG_PASS='administration_password'
##### SUPERUSER

#CREATE ROLE db_builder_restore WITH LOGIN SUPERUSER PASSWORD 'blahblahblah';
#need a user that can actually restore the database
PG_SUPER='db_admin_restore'
PG_SUPER_PASS='blahblahblah'

##### ACTIVE ENVIRONMENT
ENVIRONMENT='LOCAL'

##### ENVIRONMENT SETTINGS
export PGPASSWORD=${PG_PASS}
#loads PSQL 9.4 into path for script as opposed to 9.5
#the above is due to having 9.5 on my Mac and 9.4 on my servers
export PATH=/Applications/Postgres.app/Contents/Versions/9.4/bin:$PATH

#########################################   CONFIGURATIONS   ############################################

##### HOST CONFIGURATIONS
#PROD_SOURCE
PG_SOURCE_HOST='your source database'
PG_SOURCE_PORT='your source port'

#QA_TARGET
PG_TARGET_HOST_QA='environment based target'
PG_TARGET_PORT_QA='... port'

#DEV_TARGET
PG_TARGET_HOST_DEV='environment based target'
PG_TARGET_PORT_DEV='... port'

#LOCAL_TARGET
PG_TARGET_HOST_LOCAL='localhost'
PG_TARGET_PORT_LOCAL='5432'



#########################################   METHODS   ############################################

function verbose_notice() {
    if [ "${VERBOSE}" -eq 1 ]
        then
            echo "$1"
    fi
}


#SET ACTIVE TARGET
function set_active_target() {
    case $1 in
        local)
            PG_TARGET_HOST=$PG_TARGET_HOST_LOCAL
            PG_TARGET_PORT=$PG_TARGET_PORT_LOCAL
            ENVIRONMENT='LOCAL'
            verbose_notice "LOCAL Selected"
            ;;
        dev)
            PG_TARGET_HOST=$PG_TARGET_HOST_DEV
            PG_TARGET_PORT=$PG_TARGET_PORT_DEV
            ENVIRONMENT='DEV'
            verbose_notice "DEV Selected"
            ;;
        qa)
            PG_TARGET_HOST=$PG_TARGET_HOST_QA
            PG_TARGET_PORT=$PG_TARGET_PORT_QA
            ENVIRONMENT='QA'
            verbose_notice "QA Selected"
            ;;
        *)
            verbose_notice "Error in Environment Selection"
            verbose_notice "Defaulting to ${ENVIRONMENT}"
            exit 1
    esac
}
#function drop_old_database $1 = connection target $2 = database to drop

function drop_database () {
    verbose_notice "Dropping database $2"
    verbose_notice "Terminating Active Connections"
    terminate_active_connections "${1}" $2
    verbose_notice "Connections Terminated"
    if [ $(dropdb $1 --if-exists $2) $? -ne 0 ]
            then
            verbose_notice "Dropping of database $2 Failed"
            return 1
        else
            verbose_notice "Database $2 Dropped"
            return 0
        fi
}

#function create_new_database $1= connection target $2 = database to create.
#returns 0 if success, 1 if already exists

function create_database() {
    if psql -lqt | cut -d \| -f 1 | grep -qw $2; then
            verbose_notice "Cannot Create Database, $2 already exists"
            return 1
    else
        createdb $1 -E UTF8 $2
        psql $1 -d $2 -c 'CREATE EXTENSION pg_stat_statements;'
        psql $1 -d $2 -c 'CREATE EXTENSION btree_gist'
        #Any other specifically needed extensions, etc should be loaded here
        verbose_notice "$2 Created, beginning Restore to $2"

        pg_restore $PG_CONNECT_TARGET $PG_RESTORE_FLAGS
        if [ "$?" -ne 0 ];
            then
            verbose_notice "Restore to $2 Failed"
            return 1
        else
            verbose_notice "$2 Restored"
            return 0
        fi

    fi
}
#function terminate active connections $1 = connection string $2 = database

function terminate_active_connections() {
    verbose_notice "Terminating Connections on Database ${2}"
    declare -a connections=($(psql $1 -d postgres -t -c "select pid from pg_stat_activity where datname = '${2}'"))
    CONNECTION_COUNT=0
    TERMINATION_COUNT=0
    for connection in "${connections[@]}"
    do
        if psql $1 -t -d postgres -c "select pg_terminate_backend($connection)" -eq t; then
            verbose_notice "Connection Terminated"
            (( TERMINATION_COUNT++ ))
            (( CONNECTION_COUNT++ ))
        else
            verbose_notice "Connection Unable To Be Terminated"
            (( CONNECTION_COUNT++ ))
        fi
    done
    verbose_notice "${TERMINATION_COUNT} of ${CONNECTION_COUNT} connection(s) on Database ${2} terminated"
}

#fucntion get_database_size $1 = connection string $2 = database name
#don't mess with large databases, this isn't made for large data sets
function get_database_size() {
    DATABASE_SIZE=($(psql $1 -d postgres -t -c "SELECT pg_catalog.pg_database_size(datname) FROM pg_catalog.pg_database WHERE datname ='${2}'"))
    if [ $DATABASE_SIZE -gt 21474836480 ]; then
        verbose_notice "Database Size is too large: ${DATABASE_SIZE} bytes"
        exit 1
    fi
}

#function rename database $1 = Connection target $2 = old name $3 = new name
function rename_database () {
    if psql $1 -lqt | cut -d \| -f 1 | grep -qw $3; then
            verbose_notice "Target Database Name $3 Exists -- Rename Failed --"
            return 1
    fi
    if psql $1 -lqt | cut -d \| -f 1 | grep -qw $2; then
        COMMAND="ALTER DATABASE ${2} RENAME TO ${3}"
        terminate_active_connections "${1}" $2
        if psql $1 -d postgres -c "${COMMAND}" $? -eq 0; then
            verbose_notice "Rename of $2 to $3 Succeeded"
        fi
        return 0
    else
        #No database (First time?)
        if psql $1 -d postgres -c "${COMMAND}" $? -eq 0; then
            verbose_notice "Rename of $2 to $3 Succeeded - Initial"

        else
            verbose_notice "Source Database $2 Does not Exist -- Rename Failed --"
            return 1
        fi
    fi
}

#########################################   BEGIN MAIN   ############################################

##### START

##### SET PARAMETERS
#Parameter 1 is database
#Parameter 2 is environment

set_active_target $2

if [ "${PG_SUPER}" = "superusername" ]; then
    verbose_notice "SETUP A SUPER USER TO CREATE DATABASES CORRECTLY"
    exit 1
fi

##### DATABASE CONFIGURATIONS
#DATABASES
PG_DB="${1}"
PG_NEW_DB="${1}_new"
PG_OLD_DB="${1}_old"
##### COMMAND FLAGS

#DUMP/RESTORE SETTINGS
PG_DUMP_FILE="${PG_DB}_backup.dump"
PG_DUMP_FLAGS="-Fc -N public -f ${PG_DUMP_FILE} ${PG_DB}"
PG_DUMP="pg_dump ${PG_CONNECT_SOURCE} ${PG_DUMP_FLAGS}"
PG_RESTORE_FLAGS="-Fc -d ${PG_NEW_DB} ${PG_DUMP_FILE}"

##### SET CONNECTION STRINGS
PG_CONNECT_SOURCE="-h ${PG_SOURCE_HOST} -p ${PG_SOURCE_PORT} -U ${PG_USER}"
PG_CONNECT_TARGET="-h ${PG_TARGET_HOST} -p ${PG_TARGET_PORT} -U ${PG_USER}"
PG_RESTORE_TARGET="-h ${PG_TARGET_HOST} -p ${PG_TARGET_PORT} -U postgres"
verbose_notice "Connection Strings Set"

PSQL_VERSION=$(psql --version)
verbose_notice "${PSQL_VERSION}"
verbose_notice "Executing Data Migration Into ${ENVIRONMENT}"


##### ESCALATE PERMISSIONS
export PGPASSWORD=$PG_SUPER_PASS
psql -h $PG_TARGET_HOST -p $PG_TARGET_PORT -d postgres -U $PG_SUPER -c 'ALTER ROLE your_admin_role WITH superuser'
export PGPASSWORD=${PG_PASS}
verbose_notice "Permissions Escalated"

get_database_size "${PG_CONNECT_SOURCE}" $PG_DB


#terminate_active_connections "${PG_CONNECT_TARGET}"  $PG_DB

##### DUMP SOURCE DATA
pg_dump ${PG_CONNECT_SOURCE} ${PG_DUMP_FLAGS}

##### DROP TEMPORARY DATABASES
drop_database "${PG_CONNECT_TARGET}" $PG_OLD_DB
drop_database "${PG_CONNECT_TARGET}" $PG_NEW_DB

##### CREATE NEW TEMP DATABASE AND RESTORE

if create_database "${PG_CONNECT_TARGET}" "${PG_NEW_DB}" $? -eq 0; then
    if rename_database "${PG_CONNECT_TARGET}" $PG_DB $PG_OLD_DB $? -eq 0; then
        if rename_database "${PG_CONNECT_TARGET}" $PG_NEW_DB $PG_DB $? -eq 0; then
            verbose_notice "Execution Complete"
        else
            verbose_notice "Execution Failed at Rename ${PG_NEW_DB} to ${PG_DB}"
        fi
    else
        verbose_notice "Execution Failed at Rename ${PG_DB} to ${PG_OLD_DB}"
    fi
else
    verbose_notice "Execution Failed at Create Database ${PG_NEW_DB}"
fi

##### DE-ESCALATE PERMISSIONS
export PGPASSWORD=$PG_SUPER_PASS
psql -h $PG_TARGET_HOST -p $PG_TARGET_PORT -d postgres -U $PG_SUPER -c 'ALTER ROLE your_db_admin_role WITH nosuperuser'
export PGPASSWORD=${PG_PASS}
verbose_notice "Permissions Deescalated"
end=`date +%s`

SIZE_MB=$((DATABASE_SIZE / 1024 /1024))
verbose_notice "Database Size is: ${SIZE_MB} MB or ${DATABASE_SIZE} bytes"
runtime=$((end-start))
verbose_notice "Runtime: ${runtime} Seconds"
#########################################   END MAIN   ##############################################