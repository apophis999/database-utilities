Database Migration Script
This scrips accepts a database name (select datname) and a configured 
environment (dev, qa, or local, or whatever), dumps the latest state of production (or other source)
data, creates a new database with the name (your database)_new, restores
the executed database dump into that database.  Provided no errors
occurred, it will then terminate all active connections, rename your
existing database to (your database)_old and rename the (your database)_new
to (your database).

It is limited to databases under 20GB.  It takes around 1 minute to 
dump and restore a 200MB database.  Your observed performance may vary.

USAGE is pg_database_migration.sh database_name/datname environment flag

e.g. pg_database_migration.sh my_database_name dev

Run this on your target environment first:
CREATE ROLE some_user_can_restore WITH LOGIN SUPERUSER PASSWORD 'blah';

This allows the appropriate escalations to occur for the database restore to work.

Note, this process does not create users / roles, any dependencies on roles must be 
solved in the target environment prior running the script.

It does pull the data (pg_dump) to wherever your are running it from and restores
to the target, so there is some inefficiency in bandwidth.